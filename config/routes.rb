require 'api_constraints'

Rails.application.routes.draw do
  
  devise_for :users

  # Api definition.
  namespace :api, defaults: { format: :json }, path: '/' do
    namespace :v1 do
      scope constraints: ApiConstraints.new(version: 1, default: true) do
        resources :users, :only => [:index, :show, :create, :update, :destroy]
        resources :sessions, :only => [:create, :destroy]
        resources :foods, :only => [:show, :index, :create, :update, :destroy]
        resources :brands, :only => [:show, :index, :create, :update, :destroy]
        resources :recipes, :only => [:show, :index, :create, :update, :destroy]
      end
    end
  end

end