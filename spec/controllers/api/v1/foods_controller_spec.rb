require 'spec_helper'

describe Api::V1::FoodsController do

  describe "GET #show" do
    before(:each) do
      @food = FactoryGirl.create :food
      get :show, id: @food.id
    end

    # it "returns the information about a reporter on a hash" do
    #   food_response = json_response[:food]
    #   expect(food_response[:user][:name]).to eql @food.user.name
    # end

    it { should respond_with 200 }
  end

  describe "GET #index" do
    before(:each) do
      4.times { FactoryGirl.create :food } 
    end

    context "when is not receiving any food_ids parameter" do
      before(:each) do
        get :index
      end

      it "returns 4 records from the database" do
        foods_response = json_response
        expect(foods_response[:foods]).to have(4).items
      end

      it "returns the user object into each food" do
        foods_response = json_response[:foods]
        foods_response.each do |food_response|
          expect(food_response[:user]).to be_present
        end
      end

      it { should respond_with 200 }
    end

    context "when food_ids parameter is sent" do
      before(:each) do
        @user = FactoryGirl.create :user
        3.times { FactoryGirl.create :food, user: @user }
        get :index, food_ids: @user.food_ids
      end

      it "returns just the foods that belong to the user" do
        foods_response = json_response[:foods]
        foods_response.each do |food_response|
          expect(food_response[:user][:email]).to eql @user.email
        end
      end
    end
  end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        user = FactoryGirl.create :user
        @food_attributes = FactoryGirl.attributes_for :food
        api_authorization_header user.auth_token
        post :create, { user_id: user.id, food: @food_attributes }
      end

      it "renders the json representation for the food record just created" do
        food_response = json_response[:food]
        expect(food_response[:name]).to eql @food_attributes[:name]
      end

      it { should respond_with 201 }
    end

    context "when is not created" do
      before(:each) do
        user = FactoryGirl.create :user
        food = FactoryGirl.create :food
        @invalid_food_attributes = { brand: 1, name: 2 }
        api_authorization_header user.auth_token
        post :create, { user_id: user.id, food: @invalid_food_attributes }
      end

      it "renders an errors json" do
        food_response = json_response
        expect(food_response).to have_key(:errors)
      end

      it "renders the json errors on whye the user could not be created" do
        food_response = json_response
        expect(food_response[:errors][:calories_per_serving]).to include "is not a number"
      end

      it { should respond_with 422 }
    end
  end

  describe "PUT/PATCH #update" do
    before(:each) do
      @user = FactoryGirl.create :user
      @food = FactoryGirl.create :food, user: @user
      api_authorization_header @user.auth_token
    end

    context "when is successfully updated" do
      before(:each) do
        patch :update, { user_id: @user.id, id: @food.id,
              food: { name: "A Food Item" } }
      end

      it "renders the json representation for the updated user" do
        food_response = json_response[:food]
        expect(food_response[:name]).to eql "A Food Item"
      end

      it { should respond_with 200 }
    end

    context "when is not updated" do
      before(:each) do
        patch :update, { user_id: @user.id, id: @food.id,
              food: { calories_per_serving: "two hundred" } }
      end

      it "renders an errors json" do
        food_response = json_response
        expect(food_response).to have_key(:errors)
      end

      it "renders the json errors on whye the user could not be created" do
        food_response = json_response
        expect(food_response[:errors][:calories_per_serving]).to include "is not a number"
      end

      it { should respond_with 422 }
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      @user = FactoryGirl.create :user
      @food = FactoryGirl.create :food, user: @user
      api_authorization_header @user.auth_token
      delete :destroy, { user_id: @user.id, id: @food.id }
    end

    it { should respond_with 204 }
  end

end
