FactoryGirl.define do
  factory :food do
    brand { FFaker::Company.name }
    name { FFaker::Product.product_name }
    variation { FFaker::Color.name }
    calories_per_serving { rand() * 100 }
    carbs_per_serving { rand(100) }
    sodium_per_serving { rand(1000) }
    protein_per_serving { rand(100) }
    fat_per_serving { rand(100) }
    sugar_per_serving { rand(100) }
    user
  end
end
