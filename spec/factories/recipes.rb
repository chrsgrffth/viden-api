FactoryGirl.define do
  factory :recipe do
    name "MyString"
    meal_type 1
    description "MyString"
    servings 1
    calories_per_serving 1
    carbs_per_serving 1
    fat_per_serving 1
    sodium_per_serving 1
    protein_per_serving 1
    sugar_per_serving 1
    foods nil
  end
end
