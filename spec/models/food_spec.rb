require 'spec_helper'

describe Food do
  let(:food) { FactoryGirl.build :food }
  subject { food }

  it { should respond_to(:brand) }
  it { should respond_to(:name) }
  it { should respond_to(:variation) }
  it { should respond_to(:calories_per_serving) }
  it { should respond_to(:carbs_per_serving) }
  it { should respond_to(:sodium_per_serving) }
  it { should respond_to(:protein_per_serving) }
  it { should respond_to(:fat_per_serving) }
  it { should respond_to(:sugar_per_serving) }

  it { should belong_to(:user) }

  it { should validate_presence_of :brand }
  it { should validate_presence_of :name }
  it { should validate_presence_of :variation }
  it { should validate_numericality_of(:calories_per_serving).is_greater_than_or_equal_to(0) }
  it { should validate_numericality_of(:carbs_per_serving).is_greater_than_or_equal_to(0) }
  it { should validate_numericality_of(:sodium_per_serving).is_greater_than_or_equal_to(0) }
  it { should validate_numericality_of(:protein_per_serving).is_greater_than_or_equal_to(0) }
  it { should validate_numericality_of(:fat_per_serving).is_greater_than_or_equal_to(0) }
  it { should validate_numericality_of(:sugar_per_serving).is_greater_than_or_equal_to(0) }

end