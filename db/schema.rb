# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160626031725) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "brands", ["user_id"], name: "index_brands_on_user_id", using: :btree

  create_table "foods", force: :cascade do |t|
    t.string   "brand"
    t.string   "name"
    t.string   "variation"
    t.integer  "calories_per_serving"
    t.integer  "carbs_per_serving"
    t.integer  "sodium_per_serving"
    t.integer  "protein_per_serving"
    t.integer  "fat_per_serving"
    t.integer  "sugar_per_serving"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "brand_id"
    t.integer  "serving_size"
    t.integer  "serving_size_unit"
  end

  add_index "foods", ["user_id"], name: "index_foods_on_user_id", using: :btree

  create_table "foods_recipes", force: :cascade do |t|
    t.integer  "measurement"
    t.integer  "measurement_unit"
    t.integer  "food_id"
    t.integer  "recipe_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "foods_recipes", ["food_id"], name: "index_foods_recipes_on_food_id", using: :btree
  add_index "foods_recipes", ["recipe_id"], name: "index_foods_recipes_on_recipe_id", using: :btree

  create_table "recipes", force: :cascade do |t|
    t.string   "name"
    t.integer  "meal_type"
    t.string   "description"
    t.integer  "servings"
    t.integer  "calories_per_serving"
    t.integer  "carbs_per_serving"
    t.integer  "fat_per_serving"
    t.integer  "sodium_per_serving"
    t.integer  "protein_per_serving"
    t.integer  "sugar_per_serving"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "recipes", ["user_id"], name: "index_recipes_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "auth_token",             default: ""
    t.string   "first_name"
    t.string   "last_name"
    t.date     "birthday"
    t.integer  "sex"
    t.float    "height"
    t.float    "starting_weight"
    t.integer  "activity_level"
    t.float    "bmr"
    t.float    "daily_caloric_need"
    t.integer  "permission_level"
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "foods", "users"
  add_foreign_key "foods_recipes", "foods"
  add_foreign_key "foods_recipes", "recipes"
  add_foreign_key "recipes", "users"
end
