user = User.create(
  email: 'admin@chrisgriffith.co',
  password: '123123123',
  password_confirmation: '123123123',
  first_name: 'Chris',
  last_name: 'Griffith',
  birthday: Date.new(1991, 07, 07),
  sex: 0,
  height: 70,
  starting_weight: 160,
  activity_level: 4,
  bmr: 1788.60,
  daily_caloric_need: 3085.34,
  permission_level: 2
)

user2 = User.create(
  email: 'test@chrisgriffith.co',
  password: '123123123',
  password_confirmation: '123123123',
  first_name: 'Chris',
  last_name: 'Griffith',
  birthday: Date.new(1991, 07, 07),
  sex: 0,
  height: 70,
  starting_weight: 160,
  activity_level: 4,
  bmr: 1788.60,
  daily_caloric_need: 3085.34,
  permission_level: 1
)

brand = Brand.create(name: 'Clif Bar', user: user)
brand2 = Brand.create(name: 'Vega One', user: user)
brand3 = Brand.create(name: "Annie's Homegrown", user: user)
brand4 = Brand.create(name: "Whole Foods 365", user: user2)

Food.create(
  name: 'Energy Bar',
  variation: 'White Chocolate Macadamia Nut',
  calories_per_serving: 260, # Kcals.
  fat_per_serving: 7000, # Milligrams.
  sodium_per_serving: 230,
  carbs_per_serving: 42000,
  protein_per_serving: 9000,
  sugar_per_serving: 20000,
  serving_size: 1,
  serving_size_unit: 'bar',
  user: user,
  brand: brand
)

Food.create(
  name: 'Energy Bar',
  variation: 'Oatmeal Raisin Walnut',
  calories_per_serving: 250, # Kcals.
  fat_per_serving: 5000, # Milligrams.
  sodium_per_serving: 150,
  carbs_per_serving: 44000,
  protein_per_serving: 10000,
  sugar_per_serving: 20000,
  serving_size: 1,
  serving_size_unit: 'bar',
  user: user,
  brand: brand
)

Food.create(
  name: 'All-in-One Nutritional Shake',
  variation: 'Vanilla Chai',
  calories_per_serving: 137, # Kcals.
  fat_per_serving: 3000, # Milligrams.
  sodium_per_serving: 132,
  carbs_per_serving: 11000,
  protein_per_serving: 15000,
  sugar_per_serving: 2000,
  serving_size: 1,
  serving_size_unit: 'scoop',
  user: user,
  brand: brand2
)

Food.create(
  name: 'Macaroni & Cheese',
  variation: 'Shells & White Cheddar',
  calories_per_serving: 270,
  fat_per_serving: 45000,
  sodium_per_serving: 500,
  carbs_per_serving: 47000,
  protein_per_serving: 10000,
  sugar_per_serving: 5000,
  serving_size: 2.5,
  serving_size_unit: 'oz',
  user: user,
  brand: brand3
)

Food.create(
  name: 'Whole Almonds',
  variation: '',
  calories_per_serving: 160,
  fat_per_serving: 14000,
  sodium_per_serving: 500,
  carbs_per_serving: 47000,
  protein_per_serving: 6000,
  sugar_per_serving: 1000,
  serving_size: 0.75,
  serving_size_unit: 'cup',
  user: user,
  brand: brand4
)