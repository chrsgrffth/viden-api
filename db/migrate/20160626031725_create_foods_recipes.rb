class CreateFoodsRecipes < ActiveRecord::Migration
  def change
    create_table :foods_recipes do |t|
      t.integer :measurement
      t.integer :measurement_unit
      t.references :food, index: true, foreign_key: true
      t.references :recipe, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
