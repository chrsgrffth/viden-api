class AddsBrandIdToFood < ActiveRecord::Migration
  def change
    add_column :foods, :brand_id, :integer
  end
end
