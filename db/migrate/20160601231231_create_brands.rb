class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name
      t.integer :user_id

      t.timestamps
    end
    add_index :brands, :user_id
  end
end
