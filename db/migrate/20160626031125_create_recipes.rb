class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name
      t.integer :meal_type
      t.string :description
      t.integer :servings
      t.integer :calories_per_serving
      t.integer :carbs_per_serving
      t.integer :fat_per_serving
      t.integer :sodium_per_serving
      t.integer :protein_per_serving
      t.integer :sugar_per_serving
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
