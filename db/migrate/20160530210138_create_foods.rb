class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.string :brand
      t.string :name
      t.string :variation
      t.integer :calories_per_serving
      t.integer :carbs_per_serving
      t.integer :sodium_per_serving
      t.integer :protein_per_serving
      t.integer :fat_per_serving
      t.integer :sugar_per_serving

      t.timestamps
    end
  end
end
