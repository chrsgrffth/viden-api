class AddAttributesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :birthday, :date
    add_column :users, :sex, :integer
    add_column :users, :height, :float
    add_column :users, :starting_weight, :float
    add_column :users, :activity_level, :integer
    add_column :users, :bmr, :float
    add_column :users, :daily_caloric_need, :float
  end
end
