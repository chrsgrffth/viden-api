class AddServingSizeToFoods < ActiveRecord::Migration
  def change
    add_column :foods, :serving_size, :integer
    add_column :foods, :serving_size_unit, :integer
  end
end
