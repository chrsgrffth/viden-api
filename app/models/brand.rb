class Brand < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  belongs_to :user
  has_many :foods

  def self.build_search(keyword)
    results = []
    results.push(where("name ILIKE ?", "%#{keyword.downcase}%"))
    results.flatten
  end

  def self.search(params = {})
    if params[:q].present?
      brands = Brand.build_search(params[:q])
    else
      brands = Brand.all
    end
  end

end
