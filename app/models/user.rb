class User < ActiveRecord::Base
  before_create :generate_authentication_token!
  has_many :foods, dependent: :destroy
  has_many :brands, dependent: :destroy
  validates :auth_token, uniqueness: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :birthday, presence: true
  validates :sex, presence: true
  validates :height, presence: true
  validates :starting_weight, presence: true
  validates :activity_level, presence: true
  validates :bmr, presence: true
  validates :daily_caloric_need, presence: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

end