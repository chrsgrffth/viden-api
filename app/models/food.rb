class Food < ActiveRecord::Base
  validates :name, presence: true
  # validates :variation, presence: true
  validates :calories_per_serving, numericality: { greater_than_or_equal_to: 0 }, presence: true
  validates :carbs_per_serving, numericality: { greater_than_or_equal_to: 0 }, presence: true
  validates :sodium_per_serving, numericality: { greater_than_or_equal_to: 0 }, presence: true
  validates :protein_per_serving, numericality: { greater_than_or_equal_to: 0 }, presence: true
  validates :fat_per_serving, numericality: { greater_than_or_equal_to: 0 }, presence: true
  validates :sugar_per_serving, numericality: { greater_than_or_equal_to: 0 }, presence: true
  validates :brand_id, presence: true
  
  belongs_to :user
  belongs_to :brand

  has_many :foods_recipes
  has_many :recipes, through: :foods_recipes

  def self.build_search(keyword)
    results = []
    results.push(where("brand ILIKE ?", "%#{keyword.downcase}%"))
    results.push(where("name ILIKE ?", "%#{keyword.downcase}%"))
    results.push(where("variation ILIKE ?", "%#{keyword.downcase}%"))

    results.flatten
  end

  def self.search(params = {})
    if params[:keyword].present?
      foods = Food.build_search(params[:keyword])
    else
      foods = Food.all
    end
  end

end

# scope :above_or_equal_to_calories_per_serving, lambda { |calories_per_serving| 
#   where("calories_per_serving >= ?", calories_per_serving) 
# }