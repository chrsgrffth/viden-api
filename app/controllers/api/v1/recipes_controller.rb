class Api::V1::RecipesController < ApplicationController
  before_action :authenticate_with_token!, only: [:create, :update, :destroy]
  respond_to :json

  def index
    recipes = Recipe.all
    render json: { recipes: recipes, success: true }
  end

  def show
    recipe = Recipe.find(params[:id])

    render json: { 
      recipe: recipe.as_json(
        include: { 
          foods_recipes: { include: :food, only: [:measurement, :measurement_unit] }
        }
      ), success: true}
  end

  def create
    recipe = Recipe.new(recipe_params)
    recipe.user = current_user

    foods = params[:recipe][:foods]

    # for item in foods
    #   item = Food.find item[:id]
    #   recipe.foods << item
    # end

    if recipe.save
      for item in foods
        recipefood = FoodsRecipe.new(recipe_id: recipe.id, food_id: item[:id], measurement: item[:measurement], measurement_unit: item[:measurement_unit])
        recipefood.save
      end
      render json: recipe, status: 201, location: api_v1_recipes_url
    else
      render json: { errors: recipe.errors }, status: 422
    end
  end

  private
    def recipe_params
      params.require(:recipe).permit(
        :name,
        :meal_type,
        :description,
        :servings,
        :calories_per_serving,
        :carbs_per_serving,
        :fat_per_serving,
        :sodium_per_serving,
        :protein_per_serving,
        :sugar_per_serving,
        foods_attributes: [:id, :measurement, :measurement_unit]
      )
    end

end
