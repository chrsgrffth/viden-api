class Api::V1::UsersController < ApplicationController
  before_action :authenticate_with_token!, only: [:update, :destroy]
  respond_to :json

  def index
    users = User.all
    render json: { users: users, success: true }
  end

  def show
    user = User.find(params[:id])
    render json: { user: user, success: true}
  end

  def create
    user = User.new(user_params)

    # Generate password to be used for email sign-in.
    user.password = '123123123'
    user.password_confirmation = '123123123'

    # Defaults when creating a new user.
    user.permission_level = 1

    if user.save
      render json: user, status: 201, location: [:api, :v1, user]
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def update
    user = User.find(params[:id])

    if user.update(user_params)
      render json: user, status: 200, location: [:api, :v1, user]
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def destroy
    current_user.destroy
    head 204
  end

  private

    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :birthday, :sex, :height, :starting_weight, :activity_level, :bmr, :daily_caloric_need)
    end

end