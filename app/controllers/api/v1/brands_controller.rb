class Api::V1::BrandsController < ApplicationController
  before_action :authenticate_with_token!, only: [:create, :update, :destroy]
  respond_to :json

  def index
    brands = Brand.search(params)
    render json: { brands: brands, success: true }
  end

  def show
    brand = Brand.find(params[:id])

    # Not sure how to add this to the brand object.
    foods = Food.where(brand: brand)

    render json: { brand: brand, foods: foods, success: true}
  end

  def create
    brand = current_user.brands.build(brand_params)

    if brand.save 
      render json: brand, status: 201, location: api_v1_brands_url
    else
      render json: { errors: brand.errors }, status: 422
    end
  end

  def destroy
    brand = current_user.brands.find(params[:id])
    brand.destroy
    head 204
  end

  private
    def brand_params
      params.require(:brand).permit(:name)
    end

end