class Api::V1::FoodsController < ApplicationController
  before_action :authenticate_with_token!, only: [:create, :update, :destroy]
  respond_to :json

  def index
    foods = Food.search(params)
    render json: { foods: foods, success: true }
  end

  def show
    respond_with Food.find(params[:id])
  end

  def create
    food = current_user.foods.build(food_params)

    if food.save
      render json: food, status: 201, location: api_v1_foods_url
    else
      render json: { errors: food.errors }, status: 422
    end
  end

  def update
    food = current_user.foods.find(params[:id])
    if food.update(food_params)
      render json: food, status: 200, location: [:api, food]
    else
      render json: { errors: food.errors }, status: 422
    end
  end

  def destroy
    food = current_user.foods.find(params[:id])
    food.destroy
    head 204
  end

  private

    def food_params
      params.require(:food).permit(
        :name,
        :variation,
        :calories_per_serving,
        :carbs_per_serving,
        :sodium_per_serving,
        :protein_per_serving,
        :fat_per_serving,
        :sugar_per_serving,
        :brand_id
      )
    end

end
