class FoodSerializer < ActiveModel::Serializer
  attributes :id, :brand, :name, :variation, :calories_per_serving, :carbs_per_serving, :sodium_per_serving, :protein_per_serving, :fat_per_serving, :sugar_per_serving
  has_one :user
end
